<?php
/**
 * theme Theme Customizer
 *
 * @package theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'theme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function theme_customize_preview_js() {
	wp_enqueue_script( 'theme_customizer', get_template_directory_uri() . '/assets/scripts/customizer.js', array( 'customize-preview' ), false, true );
}
add_action( 'customize_preview_init', 'theme_customize_preview_js' );


//logo
function custom_customize_register_logo($wp_customize){

	$wp_customize->add_section('logo_scheme', array(
		'title'    => __('Logotype (header)', 'tajem'),
		'priority' => 120,
	));

	$wp_customize->add_setting('custom_theme_options[logo_image]', array(
		'default'           => '',
		'capability'        => 'edit_theme_options',
		'type'           => 'option',
	));
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'logo_image', array(
		'label'    => __('Logo Image Upload', 'tajem'),
		'section'  => 'logo_scheme',
		'settings' => 'custom_theme_options[logo_image]',
	)));


}
add_action('customize_register', 'custom_customize_register_logo');

function custom_customize_register_logo_footer($wp_customize){

	$wp_customize->add_section('logo_scheme_footer', array(
		'title'    => __('Logotype (footer)', 'tajem'),
		'priority' => 130,
	));

	$wp_customize->add_setting('custom_theme_options[logo_image_footer]', array(
		'default'           => '',
		'capability'        => 'edit_theme_options',
		'type'           => 'option',
	));
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'logo_image_footer', array(
		'label'    => __('Logo Image Upload', 'tajem'),
		'section'  => 'logo_scheme_footer',
		'settings' => 'custom_theme_options[logo_image_footer]',
	)));


}
add_action('customize_register', 'custom_customize_register_logo_footer');

function custom_customize_register_background_footer($wp_customize){

	$wp_customize->add_section('background_scheme_footer', array(
		'title'    => __('Background image (footer)', 'tajem'),
		'priority' => 130,
	));

	$wp_customize->add_setting('custom_theme_options[background_image_footer]', array(
		'default'           => '',
		'capability'        => 'edit_theme_options',
		'type'           => 'option',
	));
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'background_image_footer', array(
		'label'    => __('Background Image Upload', 'tajem'),
		'section'  => 'background_scheme_footer',
		'settings' => 'custom_theme_options[background_image_footer]',
	)));


}
add_action('customize_register', 'custom_customize_register_background_footer');