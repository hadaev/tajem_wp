<?php
function loadMore(){
	$images_prod = get_field('images_dribble', 2);
	$size = 'medium'; // (thumbnail, medium, large, full or custom size)
	$iteration = 0;
	if( $images_prod ): ?>
		<?php foreach( $images_prod as $image ):
			$iteration++;
			if ($iteration > 12):
				?>
				<div class="wrap-img"><a href="<?php echo $image['url']; ?>" data-fancybox="dribble"><img src="<?php echo $image['sizes'][$size]; ?>" alt=""></a></div>
				<?php
			endif;
		endforeach; ?>
	<?php endif;
	die();
}
add_action('wp_ajax_loadMore', 'loadMore');
add_action('wp_ajax_nopriv_loadMore', 'loadMore');
