<?php
/**
 * Created by PhpStorm.
 * User: Alexandr
 * Date: 15.11.2018
 * Time: 21:16
 */
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Footer text', 'tajem' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'tajem' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'theme_widgets_init' );

function theme_widgets_contact_data() {
	register_sidebar( array(
		'name'          => esc_html__( 'Contact data', 'tajem' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'tajem' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'theme_widgets_contact_data' );

function theme_widgets_search() {
	register_sidebar( array(
		'name'          => esc_html__( 'Search and social', 'tajem' ),
		'id'            => 'sidebar-3',
		'description'   => esc_html__( 'Add widgets here.', 'tajem' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'theme_widgets_search' );

function theme_widgets_footer_link_page() {
	register_sidebar( array(
		'name'          => esc_html__( 'Footer link page', 'tajem' ),
		'id'            => 'sidebar-4',
		'description'   => esc_html__( 'Add widgets here.', 'tajem' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'theme_widgets_footer_link_page' );

