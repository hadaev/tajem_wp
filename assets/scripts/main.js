(function ($) {
$(document).ready(function() {
    //fancy box
    $("[data-fancybox]").fancybox({
        toolbar: true,
        // and they will be placed into toolbar (class="fancybox-toolbar"` element)
        buttons: [
            "zoom",
            //"share",
            // "slideShow",
            // "fullScreen",
            // "download",
            "thumbs",
            "close"
        ],
        thumbs: {
            //     // autoStart   : false,                  // Display thumbnails on opening
            //     // hideOnClose : false,                   // Hide thumbnail grid when closing animation starts
            //     // parentEl    : '.fancybox-container',  // Container is injected into this element
            //     axis        : 'x'                     // Vertical (y) or horizontal (x) scrolling
        },
        slideShow : {
            autoStart : false,
            speed     : 2000
        },
        animationEffect : "zoom",
        animationDuration : 500
    });
    //init slider main
    $(".lazy").slick({
        lazyLoad: 'ondemand', // ondemand progressive anticipated
        infinite: true,
        arrows: false,
        dots: true
    });

    //init slider team
    $('.slider.center').slick({
        centerMode: true,
        arrows: true,
        infinite: true,
        centerPadding: 40,
        slidesToShow: 5,
        prevArrow: '<a href="" class="slick-prev" aria-label="Previous"></a>',
        nextArrow: '<a href="" class="slick-next" aria-label="Next"></a>',
        responsive: [
            {
                breakpoint: 430,
                settings: {
                    arrows: true,
                    centerMode: true,
                    centerPadding: '20px',
                    slidesToShow: 3
                }
            }
        ]
    });

    //scroll to element
    var elementMenu = $('.menu a'),
        durationAnimationScroll = 1500;

    elementMenu.click(function (e) {
        var self = $(this),
            linkTarget = $(self).attr('href');
        if(linkTarget.indexOf('/') === -1 && linkTarget.indexOf('#') !== -1){
            e.preventDefault();
        }
        // self.addClass('active');
        scrollAnimation(linkTarget);
    });
    function scrollAnimation(linkTarget){
        if(linkTarget.indexOf('#') !== -1){
            $('html, body').animate({
                scrollTop: $(linkTarget).offset().top},durationAnimationScroll);
            return false;
        }
    }
//hide menu
    $('.js-click-menu').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).parent().find('.menu').toggleClass('active');
    });
    function removeMobileMenu() {
        if($(window).innerWidth() > 640){
            $('.menu').removeClass('active');
        }
    }

    removeMobileMenu();
    $(window).resize(function () {
        removeMobileMenu();
    });

    //play video
    var $fixedLayer = $('#fixed-container');
    var srcIframe =  $fixedLayer.find('iframe').attr('src');
    $fixedLayer.find('iframe').attr('src', srcIframe + '?enablejsapi=1&');
    $fixedLayer.on('click', '.close', function (e) {
        e.preventDefault();
        $fixedLayer.fadeOut('slow', function () {
            $('body').removeAttr('style');

            $fixedLayer.find('iframe').each(function() {
                $(this)[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*'); //playVideo
            });
        });
    });

    $('.play').click(function (e) {
        e.preventDefault();
        $('body').css('overflow', 'hidden');
        $fixedLayer.fadeIn('slow');
    });

    //remove container widget wp
    $('.additional-links a').unwrap();

    //load more works
    $('#load_more').click(function(e){
        e.preventDefault();
        var self = $(this);
        var url = 'http://'+ window.location.hostname + '/wp-admin/admin-ajax.php';
        var count = 6;
        var data = {
            'action': 'loadMore',
            'count' : count
        };
        self.text('Load...');
        $.ajax({
            url: url, // обработчик
            data: data, // данные
            type:'POST', // тип запроса
            success:function(data){
                var $worksRow = $('.works__row');
                var heightElement = $worksRow.find('.wrap-img').height();
                $worksRow.append(data);
                $worksRow.find('.wrap-img').height(heightElement);
                self.css({
                    'display': 'none'
                });

            }
        });
    });

    $(window).resize(function () {
        var $worksRow = $('.works__row');
        $worksRow.find('.wrap-img[style]').removeAttr('style');
    });


});
})(jQuery);
