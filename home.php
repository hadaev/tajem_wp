<?php
/**
 * Template Name: Home
 */
?>
<?php get_header(); ?>

<section class="story" id="about">
	<div class="container display-flex flex-justify-content-between">
		<div class="story__img display-flex flex-justify-content-center">
			<div class="wrap-img">
				<?php if( get_field('image_about', 2)): ?>
				<img src="<?php the_field('image_about', 2);?>" alt="">
				<?php endif; ?>
			</div>
		</div>
		<div class="story__description">
			<h2><?php the_field('title_about', 2);?></h2>
			<?php the_field('description_about', 2);?>
			<?php if(get_field('link_button_about')):
				$link_about = get_field('link_button_about');
				?>
                <a href="<?php echo $link_about['url']; ?>" target="<?php echo $link_about['target']; ?>" class="btn"><?php echo $link_about['title']; ?></a>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="video-story mask-before color-white display-flex flex-justify-content-center flex-align-items-center" style="background: url('<?php the_field('video_image_background', 2);?>') center/cover no-repeat;">
	<div class="container display-flex flex-justify-content-center flex-align-items-center">
		<a href="" class="play"></a>
		<h3 class="play-caption"><?php the_field('video_caption', 2);?></h3>
	</div>
</section>

<section class="service" id="service">
	<div class="container">
		<h2 class=""><?php the_field('title_service', 2);?></h2>
		<?php if (get_field('subtitle_service', 2)):?>
		<span class="subtitle underline-title"><?php the_field('subtitle_service', 2);?></span>
		<?php endif; ?>

		<div class="service__blocks display-flex flex-justify-content-center">
			<?php
			if( have_rows('block_service', 2) ):
				while ( have_rows('block_service', 2) ) : the_row();?>
					<div class="service__block  display-flex flex-justify-content-center">
						<?php if(get_sub_field('image')): ?>
						<div class="wrap-img"><img src="<?php the_sub_field('image');?>" alt=""></div>
						<?php endif; ?>
						<h4><?php the_sub_field('title');?></h4>
						<p><?php the_sub_field('description');?></p>
					</div>
				<?php endwhile;
			endif;
			?>
		</div>
	</div>
</section>
<section class="team color-white mask-before" id="team" style="background: url('<?php the_field('image_background_team', 2);?>') center/cover no-repeat;">
	<div class="container">
        <h2 class=""><?php the_field('title_team', 2);?></h2>
		<?php if (get_field('subtitle_team', 2)):?>
            <span class="subtitle underline-title"><?php the_field('subtitle_team', 2);?></span>
		<?php endif; ?>
		<div class="team__blocks  display-flex flex-justify-content-center">
			<?php
			if( have_rows('block_team', 2) ):
				while ( have_rows('block_team', 2) ) : the_row();?>
                    <div class="team__block  display-flex flex-justify-content-center">
                        <div class="wrap-img">
	                        <?php if(get_sub_field('image')): ?>
                                <img src="<?php the_sub_field('image');?>" alt="">
	                        <?php endif; ?>
                        </div>
                        <h4><?php the_sub_field('name');?></h4>
                        <span><?php the_sub_field('position');?></span>
                    </div>
				<?php endwhile;
			endif;
			?>
		</div>
		<span class="team__caption"><?php the_field('caption_team', 2);?></span>
		<?php if(get_field('title_button_team', 2)):
			$link_team = get_field('title_button_team', 2);
			?>
            <a href="<?php echo $link_team['url']; ?>" target="<?php echo $link_team['target']; ?>" class="btn"><?php echo $link_team['title']; ?></a>
		<?php endif; ?>
	</div>
</section>

<section class="works" id="works">
	<div class="container">
		<h2><?php the_field('title_work', 2);?>
			<?php if(get_field('link_to_dribble', 2)):
				$link_work_d = get_field('link_to_dribble', 2);
				?>
                <a href="<?php echo $link_work_d['url']; ?>" target="<?php echo $link_work_d['target']; ?>"><?php echo $link_work_d['title']; ?></a>
			<?php endif; ?>
        </h2>
	</div>
	<div class="works__blocks display-flex flex-justify-content-center">
		<div class="works__row  display-flex flex-justify-content-center">
			<?php
			$images_prod = get_field('images_dribble', 2);
			$size = 'medium'; // (thumbnail, medium, large, full or custom size)
            $iteration = 0;
			if( $images_prod ): ?>
				<?php foreach( $images_prod as $image ):
                    $iteration++;
				if ($iteration <= 12):
                    ?>
                    <div class="wrap-img"><a href="<?php echo $image['url']; ?>" data-fancybox="dribble"><img src="<?php echo $image['sizes'][$size]; ?>" alt=""></a></div>
<!--                    <div class="wrap-img"><img src="--><?php //echo $image['sizes'][$size]; ?><!--" alt=""></div>-->
				<?php
                endif;
                endforeach; ?>
			<?php endif;?>
		</div>
	</div>
	<div class="container">
		<?php if(get_field('title_button_work', 2)):
			$link_work = get_field('title_button_work', 2);
			?>
            <a href="<?php echo $link_work['url']; ?>" target="<?php echo $link_work['target']; ?>" class="btn" id="load_more"><?php echo $link_work['title']; ?></a>
		<?php endif; ?>
	</div>
</section>

<section class="testimonials mask-before color-white" id="testimonials" style="background: url('<?php the_field('image_background_testimonials', 2);?>') center/cover no-repeat;">
	<div class="container">
		<div class="testimonials__description">
			<div class="wrap-img"><img src="<?php bloginfo('template_url' ); ?>/assets/images/quote.png" alt=""></div>
			<p>
				<?php the_field('description_client', 2);?>
			</p>
		</div>

		<div class="center slider">
			<?php
			if( have_rows('slide_client', 2) ):
				while ( have_rows('slide_client', 2) ) : the_row();?>
                    <div class="slide">
                        <div class="slide-info">
                            <h4 class="slide-name"><?php the_sub_field('name');?></h4>
                            <div class="slide-position"><?php the_sub_field('position');?></div>
                        </div>
                        <div class="wrap-img"><img src="<?php the_sub_field('image');?>" alt=""></div>
                    </div>
				<?php endwhile;
			endif;
			?>
		</div>
	</div>
</section>
<section class="contact" id="contact">
	<div class="container display-flex  flex-justify-content-between">
		<div class="contact__form">
			<h2><?php the_field('title_contact_form', 2);?></h2>
			<?php echo do_shortcode(get_field('contact_form', 2)); ?>
		</div>
		<div class="contact__client">
			<h2><?php the_field('title_partners', 2);?></h2>
			<ul class="display-flex  flex-justify-content-between flex-align-items-center">
				<?php
				if( have_rows('logo_partners', 2) ):
					while ( have_rows('logo_partners', 2) ) : the_row();?>
                        <li><a href="<?php the_sub_field('link');?>"><span class="wrap-img"><img src="<?php the_sub_field('image');?>" alt=""></span></a></li>
					<?php endwhile;
				endif;
				?>
			</ul>
		</div>
	</div>
</section>

<?php get_footer(); ?>
