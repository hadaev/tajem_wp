<footer class="footer mask-before color-white" style="background: url('<?php echo get_option('custom_theme_options')['background_image_footer']; ?>') center/cover no-repeat;">
    <div class="container">
        <div class="footer__blocks display-flex flex-justify-content-between">
            <div class="footer__block">
                <div class="wrap-img footer__logo">
                    <a href="">
	                    <?php if(get_option('custom_theme_options')['logo_image_footer']): ?>
                            <img src="<?php echo get_option('custom_theme_options')['logo_image_footer']; ?>" alt="">
	                    <?php else : ?>
                            <img src="<?php bloginfo('template_url' ); ?>/assets/images/logo.png" alt="">
	                    <?php endif; ?>
                    </a>
                </div>
                <?php dynamic_sidebar( 'sidebar-1' ); ?>
            </div>

            <div class="footer__block">
	            <?php dynamic_sidebar( 'sidebar-2' ); ?>
            </div>

            <div class="footer__block">
	            <?php dynamic_sidebar( 'sidebar-3' ); ?>
            </div>
        </div>
        <div class="footer__bottom display-flex flex-justify-content-between">
            <div class="additional-links display-flex flex-justify-content-between">
	            <?php dynamic_sidebar( 'sidebar-4' ); ?>
            </div>
            <div class="copyright">
                <span>Copyright © <?php echo date("Y"); ?> - <a href="">Tajem Creative</a></span>
            </div>
        </div>
    </div>
</footer>
<div id="fixed-container">
    <div class="video-container">
        <a href="" class="close"></a>
	    <?php the_field('video_iframe', 2);?>
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
