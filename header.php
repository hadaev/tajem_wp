<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php get_template_part( 'template-parts/head' ); ?>
</head>

<body <?php body_class("page-body"); ?>>

    <header class="header" id="home">
        <div class="header__wrap">
            <div class="container  display-flex flex-justify-content-between flex-align-items-center">
                <div class="logo">
                    <a href="#">
	                    <?php if(get_option('custom_theme_options')['logo_image']): ?>
                            <img src="<?php echo get_option('custom_theme_options')['logo_image']; ?>" alt="">
                        <?php else : ?>
                            <img src="<?php bloginfo('template_url' ); ?>/assets/images/logo.png" alt="">
                        <?php endif; ?>
                    </a>
                </div>
                <a href="" class="header__menu-icon menu-icon js-click-menu"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <div class="menu  js-menu">
	                <?php wp_nav_menu( array( 'theme_location' => 'menu-1',
                                              'menu_class' => 'display-flex flex-justify-content-between',
                                              'container' => false ) ); ?>
                </div>
            </div>
        </div>

        <div class="lazy slider">
            <?php
            // check if the repeater field has rows of data
            if( have_rows('slide_main_slider', 2) ):

            // loop through the rows of data
            while ( have_rows('slide_main_slider', 2) ) : the_row();?>

                <div class="slide">
                    <div class="wrap-img mask">
                        <img src="<?php the_sub_field('image');?>">
                    </div>
                    <div class="slide__info display-flex flex-align-items-center flex-direction-column flex-justify-content-center">
                        <?php if(get_sub_field('title')): ?>
                        <h1 class="underline-title"><?php the_sub_field('title');?></h1>
                        <?php endif; ?>
                        <p class="slide__description">
	                        <?php the_sub_field('description');?>
                        </p>
	                    <?php if(get_sub_field('button_link')):
		                    $link = get_sub_field('button_link');
                            ?>
                        <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="btn"><?php echo $link['title']; ?></a>
	                    <?php endif; ?>
                    </div>
                </div>

            <?php endwhile;

            endif;
            ?>
        </div>
    </header>
